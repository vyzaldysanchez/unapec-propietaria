﻿namespace unapec
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.accountsPage = new System.Windows.Forms.TabPage();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.accountTypesPage = new System.Windows.Forms.TabPage();
            this.currencyTypesPage = new System.Windows.Forms.TabPage();
            this.auxiliariesPage = new System.Windows.Forms.TabPage();
            this.accountingEntriesPage = new System.Windows.Forms.TabPage();
            this.paramsPage = new System.Windows.Forms.TabPage();
            this.addRegistry = new System.Windows.Forms.Button();
            this.removeBtn = new System.Windows.Forms.Button();
            this.search = new System.Windows.Forms.TextBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.reportBtn = new System.Windows.Forms.Button();
            this.tabs.SuspendLayout();
            this.accountsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.accountsPage);
            this.tabs.Controls.Add(this.accountTypesPage);
            this.tabs.Controls.Add(this.currencyTypesPage);
            this.tabs.Controls.Add(this.auxiliariesPage);
            this.tabs.Controls.Add(this.accountingEntriesPage);
            this.tabs.Controls.Add(this.paramsPage);
            this.tabs.Location = new System.Drawing.Point(12, 51);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(805, 379);
            this.tabs.TabIndex = 0;
            // 
            // accountsPage
            // 
            this.accountsPage.Controls.Add(this.dataGrid);
            this.accountsPage.Location = new System.Drawing.Point(4, 22);
            this.accountsPage.Name = "accountsPage";
            this.accountsPage.Padding = new System.Windows.Forms.Padding(3);
            this.accountsPage.Size = new System.Drawing.Size(797, 353);
            this.accountsPage.TabIndex = 0;
            this.accountsPage.Text = "Accounts";
            this.accountsPage.UseVisualStyleBackColor = true;
            // 
            // dataGrid
            // 
            this.dataGrid.AllowUserToAddRows = false;
            this.dataGrid.AllowUserToDeleteRows = false;
            this.dataGrid.AllowUserToOrderColumns = true;
            this.dataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid.Location = new System.Drawing.Point(3, 3);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.ReadOnly = true;
            this.dataGrid.Size = new System.Drawing.Size(791, 347);
            this.dataGrid.TabIndex = 2;
            this.dataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGrid_CellClick);
            this.dataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_CellContentClick);
            this.dataGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGrid_CellDoubleClick);
            this.dataGrid.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGrid_CellClick);
            this.dataGrid.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGrid_CellLeave);
            // 
            // accountTypesPage
            // 
            this.accountTypesPage.Location = new System.Drawing.Point(4, 22);
            this.accountTypesPage.Name = "accountTypesPage";
            this.accountTypesPage.Padding = new System.Windows.Forms.Padding(3);
            this.accountTypesPage.Size = new System.Drawing.Size(797, 353);
            this.accountTypesPage.TabIndex = 1;
            this.accountTypesPage.Text = "Account Types";
            this.accountTypesPage.UseVisualStyleBackColor = true;
            // 
            // currencyTypesPage
            // 
            this.currencyTypesPage.Location = new System.Drawing.Point(4, 22);
            this.currencyTypesPage.Name = "currencyTypesPage";
            this.currencyTypesPage.Size = new System.Drawing.Size(797, 353);
            this.currencyTypesPage.TabIndex = 2;
            this.currencyTypesPage.Text = "Currency Types";
            this.currencyTypesPage.UseVisualStyleBackColor = true;
            // 
            // auxiliariesPage
            // 
            this.auxiliariesPage.Location = new System.Drawing.Point(4, 22);
            this.auxiliariesPage.Name = "auxiliariesPage";
            this.auxiliariesPage.Size = new System.Drawing.Size(797, 353);
            this.auxiliariesPage.TabIndex = 3;
            this.auxiliariesPage.Text = "Auxiliaries";
            this.auxiliariesPage.UseVisualStyleBackColor = true;
            // 
            // accountingEntriesPage
            // 
            this.accountingEntriesPage.Location = new System.Drawing.Point(4, 22);
            this.accountingEntriesPage.Name = "accountingEntriesPage";
            this.accountingEntriesPage.Size = new System.Drawing.Size(797, 353);
            this.accountingEntriesPage.TabIndex = 4;
            this.accountingEntriesPage.Text = "Accounting Entries";
            this.accountingEntriesPage.UseVisualStyleBackColor = true;
            // 
            // paramsPage
            // 
            this.paramsPage.Location = new System.Drawing.Point(4, 22);
            this.paramsPage.Name = "paramsPage";
            this.paramsPage.Size = new System.Drawing.Size(797, 353);
            this.paramsPage.TabIndex = 5;
            this.paramsPage.Text = "Params";
            this.paramsPage.UseVisualStyleBackColor = true;
            // 
            // addRegistry
            // 
            this.addRegistry.Location = new System.Drawing.Point(598, 436);
            this.addRegistry.Name = "addRegistry";
            this.addRegistry.Size = new System.Drawing.Size(72, 27);
            this.addRegistry.TabIndex = 4;
            this.addRegistry.Text = "Add";
            this.addRegistry.UseVisualStyleBackColor = true;
            this.addRegistry.Click += new System.EventHandler(this.AddRegistry_Click);
            // 
            // removeBtn
            // 
            this.removeBtn.Enabled = false;
            this.removeBtn.Location = new System.Drawing.Point(132, 436);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(75, 27);
            this.removeBtn.TabIndex = 5;
            this.removeBtn.Text = "Remove";
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.RemoveBtn_Click);
            // 
            // search
            // 
            this.search.Location = new System.Drawing.Point(572, 19);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(174, 20);
            this.search.TabIndex = 6;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(752, 19);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(61, 20);
            this.searchBtn.TabIndex = 7;
            this.searchBtn.Text = "Buscar";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // reportBtn
            // 
            this.reportBtn.Location = new System.Drawing.Point(12, 16);
            this.reportBtn.Name = "reportBtn";
            this.reportBtn.Size = new System.Drawing.Size(75, 23);
            this.reportBtn.TabIndex = 8;
            this.reportBtn.Text = "Report";
            this.reportBtn.UseVisualStyleBackColor = true;
            this.reportBtn.Click += new System.EventHandler(this.reportBtn_Click);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(829, 474);
            this.Controls.Add(this.reportBtn);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.search);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.addRegistry);
            this.Controls.Add(this.tabs);
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.tabs.ResumeLayout(false);
            this.accountsPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage accountTypesPage;
        private System.Windows.Forms.TabPage accountsPage;
        private System.Windows.Forms.TabPage currencyTypesPage;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.Button addRegistry;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.TabPage auxiliariesPage;
        private System.Windows.Forms.TabPage accountingEntriesPage;
        private System.Windows.Forms.TabPage paramsPage;
        private System.Windows.Forms.TextBox search;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button reportBtn;
    }
}