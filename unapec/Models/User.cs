﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{
    public class User : BaseModel
    {
        public int Id { get; set; }
        [StringLength(64)]
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
