﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Models;
using unapec.Repositories;

namespace unapec.Components
{
    public partial class AuxiliariesForm : Form
    {
        private Auxiliary _auxiliary;
        private bool _edit;

        public AuxiliariesForm(Auxiliary auxiliary = null)
        {
            _auxiliary = auxiliary ?? new Auxiliary();
            _edit = auxiliary != null;

            InitializeComponent();
        }

        private void AuxiliariesForm_Load(object sender, EventArgs e)
        {
            if (_edit)
            {
                descriptionField.Text = _auxiliary.Description;
                activeCheckbox.Checked = _auxiliary.Active;
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (descriptionField.Text == "")
            {
                MessageBox.Show("Description field cannot be empty...");
                return;
            }

            DialogResult saveConfirmed = MessageBox.Show("Are you sure you want to save the auxiliary: " + _auxiliary.Description + "?",
                "Confirm operation!", MessageBoxButtons.YesNo);

            if (saveConfirmed.Equals(DialogResult.Yes))
            {
                if (_edit)
                {
                    AuxiliaryRepository.GetInstance().Edit(_auxiliary);
                }
                else
                {
                    AuxiliaryRepository.GetInstance().Add(_auxiliary);
                }

                this.Close();
            }
        }

        private void descriptionField_TextChanged(object sender, EventArgs e)
        {
            _auxiliary.Description = descriptionField.Text;
        }

        private void activeCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            _auxiliary.Active = activeCheckbox.Checked;
        }
    }
}
