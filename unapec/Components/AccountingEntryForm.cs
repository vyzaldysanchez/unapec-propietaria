﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Models;
using unapec.Repositories;

namespace unapec.Components
{
    public partial class AccountingEntryForm : Form
    {
        private AccountingEntry _entry;
        private int _targetAccountId;
        private float _secondAmount;
        private float _finalSecondAmount;
        private float _firstAmount;
        private float _finalFirstAmount;

        BindingSource targetGridSource;
        BindingSource secondGridSource;
        List<Acc> targetAccounts = new List<Acc>();
        List<Acc> secondAccounts = new List<Acc>();

        public AccountingEntryForm(AccountingEntry entry = null)
        {
            _entry = entry ?? new AccountingEntry();

            InitializeComponent();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AccountingEntryForm_Load(object sender, EventArgs e)
        {
            targetGridSource = new BindingSource()
            {
                DataSource = new List<Acc>()
            };
            secondGridSource = new BindingSource()
            {
                DataSource = new List<Acc>()
            };

            targetAccountsDataGrid.DataSource = targetGridSource;
            secondAccountsDataGrid.DataSource = secondGridSource;

            List<Auxiliary> auxiliaries = AuxiliaryRepository.GetInstance().GetAll().ToList();
            List<ComboItemInt> auxiliariesSource = new List<ComboItemInt>();

            foreach (Auxiliary type in auxiliaries)
            {
                auxiliariesSource.Add(new ComboItemInt() { Name = type.Description, Value = type.Id });
            }

            this.SetComboSource(auxiliaryCombo, auxiliariesSource);

            List<Account> accounts = AccountRepository.GetInstance().FindBy(ac => ac.AllowTransactions).ToList();
            List<ComboItemInt> accountsSource = new List<ComboItemInt>();

            foreach (Account account in accounts)
            {
                accountsSource.Add(new ComboItemInt() { Name = account.Description, Value = account.Id });
            }

            this.SetComboSource(accountCombo, accountsSource);

            int firstSelectedAccountId = accountsSource.First().Value;
            List<Account> targetAccounts = AccountRepository.GetInstance()
                .FindBy(ac => ac.AllowTransactions && ac.Id != firstSelectedAccountId).ToList();
            List<ComboItemInt> targetAccountsSource = new List<ComboItemInt>();

            foreach (Account account in targetAccounts)
            {
                targetAccountsSource.Add(new ComboItemInt() { Name = account.Description, Value = account.Id });
            }

            this.SetComboSource(secondAccountCombo, targetAccountsSource);

            List<ComboItemAccountMovement> movementSource = new List<ComboItemAccountMovement>
            {
                new ComboItemAccountMovement() { Name = "DB", Value = AccountMovementType.DB },
                new ComboItemAccountMovement() { Name = "CR", Value = AccountMovementType.CR }
            };
            this.SetComboSource(movementTypeCombo, movementSource);
        }

        private void SetComboSource(ComboBox combo, IList source)
        {
            combo.DataSource = source;
            combo.DisplayMember = "Name";
            combo.ValueMember = "Value";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (_finalFirstAmount != _finalSecondAmount)
            {
                string movementType = _entry.MovementType.Equals(AccountMovementType.CR) ? "credit" : "debit";
                MessageBox.Show("You are trying to " + movementType + " " + _finalFirstAmount + " but your counter part is " + _finalSecondAmount);
                return;
            }

            if (descriptionField.Text == "" || auxiliaryCombo.SelectedValue == null || movementTypeCombo.SelectedValue == null)
            {
                MessageBox.Show("Some required inputs are empty...");
                return;
            }

            if (targetGridSource.Count == 0 && secondGridSource.Count == 0)
            {
                MessageBox.Show("Some accounts must be added.");
                return;
            }

            var saveConfirmed = MessageBox.Show("Are you sure you want to save the accounting entry: " + _entry.Description + "?",
               "Confirm operation!", MessageBoxButtons.YesNo);

            if (saveConfirmed == DialogResult.Yes)
            {
                _entry.AccountingDate = DateTime.Now.ToString("yyyy-MM-dd");
                _entry.Registered = true;
                _entry.AccountingAmount = _finalFirstAmount;

                AccountingEntryRepository.GetInstance().Add(_entry);
                AccountingEntry entry = AccountingEntryRepository.GetInstance().GetLastBy(o => o.Id);

                foreach (Acc accountEntry in targetGridSource)
                {
                    Account account = AccountRepository.GetInstance().FindBy(acc => acc.Id == accountEntry.ID).First();
                    this.SetAccountBalance(accountEntry.Amount, account);
                    EntryLog log = new EntryLog()
                    {
                        AccountId = account.Id,
                        EntryId = entry.Id,
                        Amount = accountEntry.Amount,
                        MovementType = _entry.MovementType
                    };
                    EntryLogsRepository.GetInstance().Add(log);
                }

                foreach (Acc accountEntry in secondGridSource)
                {
                    Account account = AccountRepository.GetInstance().FindBy(acc => acc.Id == accountEntry.ID).First();
                    this.SetAccountBalance(accountEntry.Amount * -1, account);
                    EntryLog log = new EntryLog()
                    {
                        AccountId = account.Id,
                        EntryId = entry.Id,
                        Amount = accountEntry.Amount,
                        MovementType = _entry.MovementType.Equals(AccountMovementType.CR) ? AccountMovementType.DB : AccountMovementType.CR
                    };
                    EntryLogsRepository.GetInstance().Add(log);
                }

                this.Close();
            }
        }

        private void SetAccountBalance(double amount, Account account)
        {
            if (_entry.MovementType.Equals(AccountMovementType.CR))
            {
                account.Balance += amount;
            }
            else
            {
                account.Balance -= amount;
            }

            AccountRepository.GetInstance().Edit(account);

            try
            {
                Account parent = AccountRepository.GetInstance().FindBy(acc => acc.Id == account.ParentAccountId).First();
                this.SetAccountBalance(amount, parent);
            }
            catch (Exception e)
            {
                return;
            }
        }

        private void descriptionField_TextChanged(object sender, EventArgs e)
        {
            _entry.Description = descriptionField.Text;
        }

        private void auxiliaryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _entry.AuxiliaryId = (auxiliaryCombo.SelectedValue as ComboItemInt).Value;
            }
            catch (Exception ex)
            {
                _entry.AuxiliaryId = Int32.Parse(auxiliaryCombo.SelectedValue.ToString());
            }
        }

        private void accountCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _entry.AccountId = (accountCombo.SelectedValue as ComboItemInt).Value;
            }
            catch (Exception ex)
            {
                _entry.AccountId = Int32.Parse(accountCombo.SelectedValue.ToString());
            }

            List<Account> targetAccounts = AccountRepository.GetInstance()
                .FindBy(ac => ac.AllowTransactions && ac.Id != _entry.AccountId).ToList();
            List<ComboItemInt> targetAccountsSource = new List<ComboItemInt>();

            foreach (Account account in targetAccounts)
            {
                targetAccountsSource.Add(new ComboItemInt() { Name = account.Description, Value = account.Id });
            }

            this.SetComboSource(secondAccountCombo, targetAccountsSource);
        }

        private void movementTypeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _entry.MovementType = (movementTypeCombo.SelectedValue as ComboItemAccountMovement).Value;
            }
            catch (Exception ex)
            {
                _entry.MovementType = (AccountMovementType)Enum.Parse(typeof(AccountMovementType), movementTypeCombo.SelectedValue.ToString());
            }
        }

        private void amountField_TextChanged(object sender, EventArgs e)
        {
            try
            {
                amountField.Text = amountField.Text.Equals("") ? "0" : amountField.Text;
                _firstAmount = float.Parse(amountField.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("The amount must be a number");
                amountField.Text = _entry.AccountingAmount.ToString();
            }

        }

        private void secondAccountCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _targetAccountId = (secondAccountCombo.SelectedValue as ComboItemInt).Value;
            }
            catch (Exception ex)
            {
                _targetAccountId = Int32.Parse(secondAccountCombo.SelectedValue.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                secondAmount.Text = secondAmount.Text.Equals("") ? "0" : secondAmount.Text;
                _secondAmount = long.Parse(secondAmount.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("The amount must be a number");
                secondAmount.Text = _secondAmount.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_entry.AccountId > 0 && _firstAmount != 0)
            {
                Account account = AccountRepository.GetInstance().FindBy(ac => ac.Id == _entry.AccountId).First();
                targetGridSource.Add(new Acc()
                {
                    ID = account.Id,
                    Account = account.Description,
                    Amount = _firstAmount
                });

                _finalFirstAmount += _firstAmount;
                _firstAmount = 0;
                amountField.Text = "";
            }
        }

        private void secondAddBtn_Click(object sender, EventArgs e)
        {
            if (_targetAccountId > 0 && _secondAmount != 0)
            {
                Account account = AccountRepository.GetInstance().FindBy(ac => ac.Id == _targetAccountId).First();
                secondGridSource.Add(new Acc()
                {
                    ID = account.Id,
                    Account = account.Description,
                    Amount = _secondAmount
                });

                _finalSecondAmount += _secondAmount;
                _secondAmount = 0;
                secondAmount.Text = "";
            }
        }

        private void OnSecondAccountRemoved(object sender, EventArgs e)
        {
            DataGridView dataGridView = (sender as DataGridView);
            _finalSecondAmount -= float.Parse(dataGridView.SelectedRows[0].Cells[2].Value.ToString());
        }

        private void OnTargetAccountRemoved(object sender, EventArgs e)
        {
            DataGridView dataGridView = (sender as DataGridView);
            _finalFirstAmount -= float.Parse(dataGridView.SelectedRows[0].Cells[2].Value.ToString());
        }
    }

    class Acc
    {
        public int ID { get; set; }
        public string Account { get; set; }
        public float Amount { get; set; }
    }
}
