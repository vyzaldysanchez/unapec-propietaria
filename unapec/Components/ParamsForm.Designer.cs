﻿namespace unapec.Components
{
    partial class ParamsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.closingMonth = new System.Windows.Forms.DateTimePicker();
            this.rnc = new System.Windows.Forms.TextBox();
            this.proccessedCheck = new System.Windows.Forms.CheckBox();
            this.proccessMonth = new System.Windows.Forms.DateTimePicker();
            this.proccessYear = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cancelBtn);
            this.groupBox1.Controls.Add(this.saveBtn);
            this.groupBox1.Controls.Add(this.closingMonth);
            this.groupBox1.Controls.Add(this.rnc);
            this.groupBox1.Controls.Add(this.proccessedCheck);
            this.groupBox1.Controls.Add(this.proccessMonth);
            this.groupBox1.Controls.Add(this.proccessYear);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 289);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Closing Tax Month";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "RNC";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Proccess Month";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Proccess Year";
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(6, 251);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 6;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(270, 251);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 5;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // closingMonth
            // 
            this.closingMonth.CustomFormat = "MM";
            this.closingMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.closingMonth.Location = new System.Drawing.Point(160, 193);
            this.closingMonth.Name = "closingMonth";
            this.closingMonth.Size = new System.Drawing.Size(85, 20);
            this.closingMonth.TabIndex = 4;
            this.closingMonth.ValueChanged += new System.EventHandler(this.closingMonth_ValueChanged);
            // 
            // rnc
            // 
            this.rnc.Location = new System.Drawing.Point(160, 153);
            this.rnc.Name = "rnc";
            this.rnc.Size = new System.Drawing.Size(135, 20);
            this.rnc.TabIndex = 3;
            this.rnc.TextChanged += new System.EventHandler(this.rnc_TextChanged);
            // 
            // proccessedCheck
            // 
            this.proccessedCheck.AutoSize = true;
            this.proccessedCheck.Location = new System.Drawing.Point(160, 123);
            this.proccessedCheck.Name = "proccessedCheck";
            this.proccessedCheck.Size = new System.Drawing.Size(76, 17);
            this.proccessedCheck.TabIndex = 2;
            this.proccessedCheck.Text = "Processed";
            this.proccessedCheck.UseVisualStyleBackColor = true;
            this.proccessedCheck.CheckedChanged += new System.EventHandler(this.proccessedCheck_CheckedChanged);
            // 
            // proccessMonth
            // 
            this.proccessMonth.CustomFormat = "MM";
            this.proccessMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.proccessMonth.Location = new System.Drawing.Point(160, 84);
            this.proccessMonth.Name = "proccessMonth";
            this.proccessMonth.Size = new System.Drawing.Size(85, 20);
            this.proccessMonth.TabIndex = 1;
            this.proccessMonth.ValueChanged += new System.EventHandler(this.proccessMonth_ValueChanged);
            // 
            // proccessYear
            // 
            this.proccessYear.CustomFormat = "yyyy";
            this.proccessYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.proccessYear.Location = new System.Drawing.Point(160, 47);
            this.proccessYear.Name = "proccessYear";
            this.proccessYear.Size = new System.Drawing.Size(85, 20);
            this.proccessYear.TabIndex = 0;
            this.proccessYear.ValueChanged += new System.EventHandler(this.proccessYear_ValueChanged);
            // 
            // ParamsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(375, 312);
            this.Controls.Add(this.groupBox1);
            this.Name = "ParamsForm";
            this.Text = "ParamsForm";
            this.Load += new System.EventHandler(this.ParamsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker proccessYear;
        private System.Windows.Forms.DateTimePicker proccessMonth;
        private System.Windows.Forms.CheckBox proccessedCheck;
        private System.Windows.Forms.TextBox rnc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.DateTimePicker closingMonth;
        private System.Windows.Forms.Label label4;
    }
}