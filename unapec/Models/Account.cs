﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{
    public enum Level
    {
        Parent, SubParent, Child
    }


    public class Account : BaseModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int AccountTypeId { get; set; }
        public bool AllowTransactions { get; set; }
        public Level Level { get; set; }
        public int? ParentAccountId { get; set; }
        public double Balance { get; set; }
        public bool Status { get; set; }

        public virtual ICollection<AccountingEntry> AccountEntry { get; set; }

    }
}
