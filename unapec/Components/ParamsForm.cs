﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Repositories;

namespace unapec.Components
{
    public partial class ParamsForm : Form
    {
        private Params _params;
        private bool _edit;

        public ParamsForm(Params paramsModel = null)
        {
            _edit = paramsModel != null;
            _params = paramsModel ?? new Params();

            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void proccessYear_ValueChanged(object sender, EventArgs e)
        {
            _params.ProcessYear = proccessYear.Value.Year;
        }

        private void proccessMonth_ValueChanged(object sender, EventArgs e)
        {
            _params.ProcessMonth = proccessMonth.Value.Month;
        }

        private void proccessedCheck_CheckedChanged(object sender, EventArgs e)
        {
            _params.WholesaleProcessed = proccessedCheck.Checked;
        }

        private void rnc_TextChanged(object sender, EventArgs e)
        {
            _params.BusinessRNC = rnc.Text;
        }

        private void closingMonth_ValueChanged(object sender, EventArgs e)
        {
            _params.ClosingTaxMonth = closingMonth.Value.Month;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            _params.ProcessYear = proccessYear.Value.Year;
            _params.ProcessMonth = proccessMonth.Value.Month;
            _params.ClosingTaxMonth = closingMonth.Value.Month;
            _params.WholesaleProcessed = proccessedCheck.Checked;

            if (_params.ProcessYear == 0 || _params.ProcessMonth == 0 || _params.BusinessRNC == "" || _params.ClosingTaxMonth == 0)
            {
                MessageBox.Show("Some required fields are empty...");
                return;
            }

            DialogResult saveConfirmed = MessageBox.Show("Are you sure you want to save the params for the RNC: " + _params.BusinessRNC + "?",
              "Confirm operation!", MessageBoxButtons.YesNo);

            if (saveConfirmed == DialogResult.Yes)
            {
                if (_edit)
                {
                    ParamsRepository.GetInstance().Edit(_params);
                }
                else
                {
                    ParamsRepository.GetInstance().Add(_params);
                }
            }

            this.Close();
        }

        private void ParamsForm_Load(object sender, EventArgs e)
        {
            proccessYear.Value = new DateTime(_params.ProcessYear, 12, 1);
            proccessMonth.Value = new DateTime(2017, _params.ProcessMonth, 1);
            closingMonth.Value = new DateTime(2017, _params.ClosingTaxMonth, 1);
            proccessedCheck.Checked = _params.WholesaleProcessed;
        }
    }
}
