﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Components;
using unapec.Models;
using unapec.Repositories;
using unapec.Repositories.Interfaces;

namespace unapec
{
    public partial class Dashboard : Form
    {
        BindingSource gridSource;

        public Dashboard()
        {
            InitializeComponent();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            gridSource = new BindingSource { DataSource = AccountRepository.GetInstance().GetAll().ToList() };
            dataGrid.DataSource = gridSource;
            tabs.Selected += new TabControlEventHandler(TabsControl_Selected);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TabsControl_Selected(object sender, EventArgs e)
        {
            TabPage page = (sender as TabControl).SelectedTab;
            addRegistry.Enabled = true;
            search.Enabled = false;
            searchBtn.Enabled = false;

            if (page.Equals(tabs.TabPages["accountsPage"]))
            {
                removeBtn.Enabled = true;
                List<Account> accounts = AccountRepository.GetInstance().GetAll().ToList();
                SetUpDataGridForSource(accounts, accountsPage);
                return;
            }

            if (page.Equals(tabs.TabPages["accountTypesPage"]))
            {
                removeBtn.Enabled = true;
                List<AccountType> accountTypes = AccountTypeRepository.GetInstance().GetAll().ToList();
                SetUpDataGridForSource(accountTypes, accountTypesPage);
                return;
            }

            if (page.Equals(tabs.TabPages["currencyTypesPage"]))
            {
                removeBtn.Enabled = true;
                List<CurrencyType> currencyTypes = CurrencyTypeRepository.GetInstance().GetAll().ToList();
                SetUpDataGridForSource(currencyTypes, currencyTypesPage);
                return;
            }

            if (page.Equals(tabs.TabPages["auxiliariesPage"]))
            {
                removeBtn.Enabled = true;
                List<Auxiliary> auxiliaries = AuxiliaryRepository.GetInstance().GetAll().ToList();
                SetUpDataGridForSource(auxiliaries, auxiliariesPage);
                return;
            }

            if (page.Equals(tabs.TabPages["accountingEntriesPage"]))
            {
                removeBtn.Enabled = false;
                search.Enabled = true;
                searchBtn.Enabled = true;
                List<AccountingEntry> accountingEntries = AccountingEntryRepository.GetInstance().GetAll().ToList();
                SetUpDataGridForSource(accountingEntries, accountingEntriesPage);

                try
                {
                    Params @params = ParamsRepository.GetInstance().GetAll().First();

                    if (@params == null || @params.WholesaleProcessed)
                    {
                        addRegistry.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    addRegistry.Enabled = false;
                }

                return;
            }

            if (page.Equals(tabs.TabPages["paramsPage"]))
            {
                removeBtn.Enabled = true;
                List<Params> @params = ParamsRepository.GetInstance().GetAll().ToList();
                SetUpDataGridForSource(@params, paramsPage);

                if (@params.Count() == 1)
                {
                    addRegistry.Enabled = false;
                }

                return;
            }

        }

        private void SetUpDataGridForSource(IList source, TabPage tabPage)
        {
            AddDataGridTo(tabPage);
            SetPageDataGrid(tabPage, source);
        }

        private void AddDataGridTo(TabPage tabPage)
        {
            if (!tabPage.Controls.Contains(dataGrid))
            {
                tabPage.Controls.Add(dataGrid);
            }
        }

        private void SetPageDataGrid(TabPage tabPage, IList source)
        {
            gridSource.DataSource = source;
            dataGrid.DataSource = gridSource;
            dataGrid.Refresh();
            gridSource.ResetBindings(true);
        }

        private void AddRegistry_Click(object sender, EventArgs e)
        {
            this.Enabled = false;

            if (tabs.SelectedTab.Equals(tabs.TabPages["accountsPage"]))
            {
                ShowActionForm(new AccountsForm());
                return;
            }

            if (tabs.SelectedTab.Equals(tabs.TabPages["accountTypesPage"]))
            {
                ShowActionForm(new AccountTypesForm());
                return;
            }

            if (tabs.SelectedTab.Equals(tabs.TabPages["currencyTypesPage"]))
            {
                ShowActionForm(new CurrencyTypeForm());
                return;
            }

            if (tabs.SelectedTab.Equals(tabs.TabPages["auxiliariesPage"]))
            {
                ShowActionForm(new AuxiliariesForm());
                return;
            }

            if (tabs.SelectedTab.Equals(tabs.TabPages["accountingEntriesPage"]))
            {
                ShowActionForm(new AccountingEntryForm());
                return;
            }

            if (tabs.SelectedTab.Equals(tabs.TabPages["paramsPage"]))
            {
                ShowActionForm(new ParamsForm());
                return;
            }

        }

        private void ShowActionForm(Form form)
        {
            form.FormClosing += Form_FormClosing;
            form.ShowDialog();
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs args)
        {
            TabsControl_Selected(tabs, args);
            this.Enabled = true;
        }

        private void DataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            TabPage page = tabs.SelectedTab;

            if (page == null || page.Equals(tabs.TabPages["accountingEntriesPage"]))
            {
                return;
            }

            this.Enabled = false;

            if (page.Equals(tabs.TabPages["accountsPage"]))
            {
                Account account = this.GetAccountFromRow(this.dataGrid.CurrentRow);
                ShowActionForm(new AccountsForm(account));
                return;
            }

            if (page.Equals(tabs.TabPages["accountTypesPage"]))
            {
                AccountType accountTypeFromRow = this.GetAccountTypeFromRow(this.dataGrid.CurrentRow);
                ShowActionForm(new AccountTypesForm(accountTypeFromRow));
                return;
            }

            if (page.Equals(tabs.TabPages["currencyTypesPage"]))
            {
                CurrencyType currencyTypeFromRow = this.GetCurrencyTypeFromRow(this.dataGrid.CurrentRow);
                ShowActionForm(new CurrencyTypeForm(currencyTypeFromRow));
                return;
            }

            if (page.Equals(tabs.TabPages["auxiliariesPage"]))
            {
                Auxiliary auxiliaryFromRow = this.GetAuxiliaryFromRow(this.dataGrid.CurrentRow);
                ShowActionForm(new AuxiliariesForm(auxiliaryFromRow));
                return;
            }

            if (page.Equals(tabs.TabPages["paramsPage"]))
            {
                Params paramsFromRow = this.GetParamsFromRow(this.dataGrid.CurrentRow);
                ShowActionForm(new ParamsForm(paramsFromRow));
                return;
            }
        }

        private void DataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!this.removeBtn.Enabled)
            {
                this.removeBtn.Enabled = true;
            }
        }

        private void DataGrid_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            /*if (this.removeBtn.Enabled)
            {
               this.removeBtn.Enabled = false;
            }*/
        }

        private Account GetAccountFromRow(DataGridViewRow row)
        {
            return new Account()
            {
                Id = Int32.Parse(row.Cells[0].Value.ToString()),
                Description = row.Cells[1].Value.ToString(),
                AccountTypeId = Int32.Parse(row.Cells[2].Value.ToString()),
                AllowTransactions = Boolean.Parse(row.Cells[3].Value.ToString()),
                Level = (Level)Enum.Parse(typeof(Level), row.Cells[4].Value.ToString()),
                ParentAccountId = row.Cells[5].Value != null ? Int32.Parse(row.Cells[5].Value.ToString()) : 0,
                Balance = double.Parse(row.Cells[6].Value.ToString()),
                Status = Boolean.Parse(row.Cells[7].Value.ToString())
            };
        }

        private AccountType GetAccountTypeFromRow(DataGridViewRow row)
        {
            return new AccountType()
            {
                Id = Int32.Parse(row.Cells[0].Value.ToString()),
                Description = row.Cells[1].Value.ToString(),
                Origin = (AccountMovementType)Enum.Parse(typeof(AccountMovementType), row.Cells[2].Value.ToString()),
                Active = (bool)row.Cells[3].Value
            };
        }

        private CurrencyType GetCurrencyTypeFromRow(DataGridViewRow row)
        {
            return new CurrencyType()
            {
                Id = Int32.Parse(row.Cells[0].Value.ToString()),
                Description = row.Cells[1].Value.ToString(),
                LastExchangeRate = double.Parse(row.Cells[2].Value.ToString()),
                Status = (bool)row.Cells[3].Value
            };
        }


        private Auxiliary GetAuxiliaryFromRow(DataGridViewRow row)
        {
            return new Auxiliary()
            {
                Id = Int32.Parse(row.Cells[0].Value.ToString()),
                Description = row.Cells[1].Value != null ? row.Cells[1].Value.ToString() : "",
                Active = (bool)row.Cells[2].Value
            };
        }


        private AccountingEntry GetAccountingEntriesFromRow(DataGridViewRow row)
        {
            return new AccountingEntry()
            {
                Id = Int32.Parse(row.Cells[0].Value.ToString()),
                Description = row.Cells[1].Value != null ? row.Cells[1].Value.ToString() : "",
                AuxiliaryId = Int32.Parse(row.Cells[2].Value.ToString()),
                AccountId = Int32.Parse(row.Cells[3].Value.ToString()),
                MovementType = (AccountMovementType)Enum.Parse(typeof(AccountMovementType), row.Cells[4].Value.ToString()),
                AccountingDate = row.Cells[5].Value.ToString(),
                AccountingAmount = Int32.Parse(row.Cells[6].Value.ToString()),
                Registered = (bool)row.Cells[7].Value
            };
        }

        private Params GetParamsFromRow(DataGridViewRow row)
        {
            return new Params()
            {
                Id = Int32.Parse(row.Cells[0].Value.ToString()),
                ProcessYear = Int32.Parse(row.Cells[1].Value.ToString()),
                ProcessMonth = Int32.Parse(row.Cells[2].Value.ToString()),
                WholesaleProcessed = (bool)row.Cells[3].Value,
                BusinessRNC = row.Cells[4].Value.ToString(),
                ClosingTaxMonth = Int32.Parse(row.Cells[5].Value.ToString())
            };
        }

        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            TabPage page = tabs.SelectedTab;
            if (page != null)
            {
                if (page.Equals(tabs.TabPages["accountsPage"]))
                {
                    Account rowAccount = GetAccountFromRow(this.dataGrid.CurrentRow);
                    Account account = AccountRepository.GetInstance()
                        .FindBy(ac => ac.Id == rowAccount.Id).First();

                    var deletionConfirmed = MessageBox.Show("Are you sure you want to remove the account: " + account.Description + "?",
                            "Confirm operation!", MessageBoxButtons.YesNo);

                    if (deletionConfirmed == DialogResult.No)
                    {
                        return;
                    }

                    AccountRepository.GetInstance().Delete(account);
                    this.TabsControl_Selected(tabs, e);
                    return;
                }

                if (page.Equals(tabs.TabPages["accountTypesPage"]))
                {

                    AccountType rowAccount = GetAccountTypeFromRow(this.dataGrid.CurrentRow);
                    AccountType accountType = AccountTypeRepository.GetInstance()
                        .FindBy(ac => ac.Id == rowAccount.Id).First();

                    var deletionConfirmed = MessageBox.Show("Are you sure you want to remove the account type: " + accountType.Description + "?",
                            "Confirm operation!", MessageBoxButtons.YesNo);

                    if (deletionConfirmed == DialogResult.No)
                    {
                        return;
                    }

                    AccountTypeRepository.GetInstance().Delete(accountType);
                    this.TabsControl_Selected(tabs, e);
                    return;
                }

                if (page.Equals(tabs.TabPages["currencyTypesPage"]))
                {
                    CurrencyType rowCurrencyType = GetCurrencyTypeFromRow(this.dataGrid.CurrentRow);
                    CurrencyType currentyType = CurrencyTypeRepository.GetInstance()
                        .FindBy(ac => ac.Id == rowCurrencyType.Id).First();

                    var deletionConfirmed = MessageBox.Show("Are you sure you want to remove the currency type: " + currentyType.Description + "?",
                            "Confirm operation!", MessageBoxButtons.YesNo);

                    if (deletionConfirmed == DialogResult.No)
                    {
                        return;
                    }

                    CurrencyTypeRepository.GetInstance().Delete(currentyType);
                    this.TabsControl_Selected(tabs, e);
                    return;
                }

                if (page.Equals(tabs.TabPages["auxiliariesPage"]))
                {
                    Auxiliary rowAuxiliary = GetAuxiliaryFromRow(this.dataGrid.CurrentRow);
                    Auxiliary auxiliary = AuxiliaryRepository.GetInstance()
                        .FindBy(ac => ac.Id == rowAuxiliary.Id).First();

                    var deletionConfirmed = MessageBox.Show("Are you sure you want to remove the auxiliary: " + auxiliary.Description + "?",
                            "Confirm operation!", MessageBoxButtons.YesNo);

                    if (deletionConfirmed == DialogResult.No)
                    {
                        return;
                    }

                    AuxiliaryRepository.GetInstance().Delete(auxiliary);
                    this.TabsControl_Selected(tabs, e);
                    return;
                }

                if (page.Equals(tabs.TabPages["accountingEntriesPage"]))
                {
                    AccountingEntry rowAccountingEntry = GetAccountingEntriesFromRow(this.dataGrid.CurrentRow);
                    AccountingEntry entry = AccountingEntryRepository.GetInstance()
                        .FindBy(ac => ac.Id == rowAccountingEntry.Id).First();

                    var deletionConfirmed = MessageBox.Show("Are you sure you want to remove the entry: " + entry.Description + "?",
                            "Confirm operation!", MessageBoxButtons.YesNo);

                    if (deletionConfirmed == DialogResult.No)
                    {
                        return;
                    }

                    AccountingEntryRepository.GetInstance().Delete(entry);
                    this.TabsControl_Selected(tabs, e);
                    return;
                }

                if (page.Equals(tabs.TabPages["paramsPage"]))
                {
                    Params paramsFromRow = GetParamsFromRow(this.dataGrid.CurrentRow);
                    Params @params = ParamsRepository.GetInstance()
                        .FindBy(ac => ac.Id == paramsFromRow.Id).First();

                    var deletionConfirmed = MessageBox.Show("Are you sure you want to remove the params with RNC: " + @params.BusinessRNC + "?",
                            "Confirm operation!", MessageBoxButtons.YesNo);

                    if (deletionConfirmed == DialogResult.No)
                    {
                        return;
                    }

                    ParamsRepository.GetInstance().Delete(@params);
                    this.TabsControl_Selected(tabs, e);
                    return;
                }
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {

        }

        private void dataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            List<AccountingEntry> data;

            if (search.Text.Equals(""))
            {
                data = AccountingEntryRepository.GetInstance().GetAll().ToList();
            }
            else
            {
                data = AccountingEntryRepository.GetInstance()
                    .FindBy((acc) => acc.Description.IndexOf(search.Text) != -1).ToList();
            }

            SetUpDataGridForSource(data, accountingEntriesPage);
        }

        private void reportBtn_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            var reportForm = new ReportForm();
            reportForm.FormClosing += this.EnableForm;
            reportForm.Show();
            
        }

        private void EnableForm(object sender, FormClosingEventArgs args)
        {
            this.Enabled = true;
        }
    }
}
