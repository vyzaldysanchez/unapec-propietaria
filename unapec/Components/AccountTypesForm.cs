﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Repositories;

namespace unapec.Components
{
    public partial class AccountTypesForm : Form
    {
        private AccountType _accountType;
        private bool _edit = false;

        public AccountTypesForm(AccountType accountType = null)
        {
            _accountType = accountType ?? new AccountType();
            _edit = accountType != null;

            InitializeComponent();
        }

        private void AccountTypesForm_Load(object sender, EventArgs e)
        {
            List<ComboItemAccountMovement> originSource = new List<ComboItemAccountMovement>
            {
                new ComboItemAccountMovement() { Name = "DB", Value = AccountMovementType.DB },
                new ComboItemAccountMovement() { Name = "CR", Value = AccountMovementType.CR }
            };

            AccountMovementType val = _accountType.Origin;

            this.SetComboSource(originComboBox, originSource);

            if (_edit)
            {
                descriptionField.Text = _accountType.Description;
                originComboBox.SelectedValue = val;
                statusCheck.Checked = _accountType.Active;
            }
        }

        private void SetComboSource(ComboBox combo, IList source)
        {
            combo.DataSource = source;
            combo.DisplayMember = "Name";
            combo.ValueMember = "Value";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (descriptionField.Text == "")
            {
                MessageBox.Show("You must put a description");
                return;
            }

            DialogResult saveConfirmed = MessageBox.Show("Are you sure you want to save the account type: " + _accountType.Description + "?",
                "Confirm operation!", MessageBoxButtons.YesNo);

            if (saveConfirmed.Equals(DialogResult.Yes))
            {
                if (_edit)
                {
                    AccountTypeRepository.GetInstance().Edit(_accountType);
                }
                else
                {
                    AccountTypeRepository.GetInstance().Add(_accountType);
                }


                this.Close();
            }
        }

        private void descriptionField_TextChanged(object sender, EventArgs e)
        {
            _accountType.Description = descriptionField.Text;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void originComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _accountType.Origin = (originComboBox.SelectedValue as ComboItemAccountMovement).Value;
            }
            catch (Exception ex)
            {
                _accountType.Origin = (AccountMovementType)Enum.Parse(typeof(AccountMovementType), originComboBox.SelectedValue.ToString());
            }
        }

        private void statusCheck_CheckedChanged(object sender, EventArgs e)
        {
            _accountType.Active = statusCheck.Checked;
        }
    }
}