﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Components;

namespace unapec
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void OnCancelButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OnLoginButtonClick(object sender, EventArgs e)
        {
            if (userName.Text == "" || password.Text == "")
            {
                MessageBox.Show("Username and password fields must not be empty. Try again.");
                return;
            }

            UserRepository usersRepository = new UserRepository();

            this.Hide();

            LoaderMessageDialog loaderDialog = new LoaderMessageDialog();
            loaderDialog.Show();
            loaderDialog.Refresh();

            bool userExists = usersRepository.Authenticate(userName.Text, password.Text);

            if (userExists)
            {
                loaderDialog.Close();
                Dashboard dashboard = new Dashboard();
                dashboard.ShowDialog(this);
                this.Close();
            }
            else
            {
                loaderDialog.Close();
                MessageBox.Show("User not found, check your username/password and try again.");
                this.Show();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
