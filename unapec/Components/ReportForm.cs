﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace unapec.Components
{
    public partial class ReportForm : Form
    {
        public ReportForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            Report report = new Report(fromDate.Value, toDate.Value);
            report.Show();
            report.FormClosing += this.EnableForm;
            report.FormClosed += this.EnableForm;
        }

        public void EnableForm(object sender, EventArgs args)
        {
            this.Enabled = true;
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            fromDate.Value = new DateTime(2016, 06, 01);
            toDate.Value = new DateTime(2017, 07, 01);
        }
    }
}
