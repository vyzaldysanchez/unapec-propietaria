﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Repositories;

namespace unapec
{
    class UserRepository : BaseRepository<DBContext, User>
    {
        public bool Authenticate(string username, string password)
        {
            return FindBy(user => user.UserName == username && user.Password == password).FirstOrDefault() != null;
        }
    }
}
