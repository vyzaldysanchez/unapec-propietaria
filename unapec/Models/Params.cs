﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{
    public class Params : BaseModel
    {
        public int Id { get; set; }
        public int ProcessYear { get; set; }
        public int ProcessMonth { get; set; }
        public bool WholesaleProcessed { get; set; }
        public string BusinessRNC { get; set; }
        public int ClosingTaxMonth { get; set; }
    }
}
