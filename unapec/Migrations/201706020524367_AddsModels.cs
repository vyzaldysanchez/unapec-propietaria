namespace unapec.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddsModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        AccountTypeId = c.Int(nullable: false),
                        AllowTransactions = c.Boolean(nullable: false),
                        ParentAccountId = c.Int(nullable: false),
                        Balance = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Accounts", t => t.ParentAccountId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.ParentAccountId);
            
            CreateTable(
                "dbo.AccountTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "ParentAccountId", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.Accounts", new[] { "ParentAccountId" });
            DropIndex("dbo.Accounts", new[] { "AccountTypeId" });
            DropTable("dbo.AccountTypes");
            DropTable("dbo.Accounts");
        }
    }
}
