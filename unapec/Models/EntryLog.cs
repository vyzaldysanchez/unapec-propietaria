﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace unapec.Models
{
    public class EntryLog
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int EntryId { get; set; }
        public float Amount { get; set; }
        public AccountMovementType MovementType { get; set; }
    }
}
