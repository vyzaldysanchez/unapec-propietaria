﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{
    public class Wholesale : BaseModel
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int ParentAccountId { get; set; }
        public enum MovementType { DB, CR }
        public DateTime ProcessingDate { get; set; }
        public long Balance { get; set; }
        public enum Status { Active, Inactive }
    }
}
