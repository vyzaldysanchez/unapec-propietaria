﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Repositories.Interfaces;

namespace unapec.Repositories
{
    public class BaseRepository<C, T> : IGenericRepository<T> where T : class where C : DBContext
    {
        private C _entities = (C)DBContext.GetInstance();
        private static BaseRepository<C, T> _instance;

        public C Context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        protected BaseRepository()
        {
        }

        public static BaseRepository<C, T> GetInstance()
        {
            if (_instance == null)
            {
                _instance = new BaseRepository<C, T>();
            }

            return _instance;
        }

        public virtual IQueryable<T> GetAll()
        {

            IQueryable<T> query = _entities.Set<T>();
            return query;
        }

        public IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {

            IQueryable<T> query = _entities.Set<T>().Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            _entities.Set<T>().Add(entity);
            this.Save();
        }

        public virtual void Delete(T entity)
        {
            _entities.Set<T>().Remove(entity);
            this.Save();
        }

        public virtual void Edit(T entity)
        {
            _entities.Set<T>().AddOrUpdate(entity);
            this.Save();
        }

        public virtual T GetLastBy(System.Linq.Expressions.Expression<Func<T, int>> predicate)
        {
            return _entities.Set<T>().OrderByDescending(predicate).FirstOrDefault();
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }
    }
}
