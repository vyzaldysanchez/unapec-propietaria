﻿namespace unapec.Components
{
    partial class LoaderMessageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loaderMessageLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loaderMessageLabel
            // 
            this.loaderMessageLabel.AutoSize = true;
            this.loaderMessageLabel.Location = new System.Drawing.Point(85, 38);
            this.loaderMessageLabel.Name = "loaderMessageLabel";
            this.loaderMessageLabel.Size = new System.Drawing.Size(119, 13);
            this.loaderMessageLabel.TabIndex = 0;
            this.loaderMessageLabel.Text = "Please wait a moment...";
            // 
            // LoaderMessageDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(286, 85);
            this.Controls.Add(this.loaderMessageLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoaderMessageDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoaderMessage";
            this.Load += new System.EventHandler(this.loaderMessage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label loaderMessageLabel;
    }
}