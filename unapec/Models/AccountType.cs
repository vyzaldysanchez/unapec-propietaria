﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{
    public class AccountType : BaseModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public AccountMovementType Origin { get; set; }
        public bool Active { get; set; }
    }
}
