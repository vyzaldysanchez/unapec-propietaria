﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace unapec.Repositories
{
    public class AccountRepository : BaseRepository<DBContext, Account>
    {
        private static AccountRepository _instance;

        public static new AccountRepository GetInstance()
        {
            if (_instance == null)
            {
                _instance = new AccountRepository();
            }

            return _instance;
        }


        public IQueryable<Account> GetParentAccounts()
        {
            return FindBy(account => account.Level == Level.Parent || account.Level == Level.SubParent);
        }
    }
}
