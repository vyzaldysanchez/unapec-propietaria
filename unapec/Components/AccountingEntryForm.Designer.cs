﻿using System.Windows.Forms;

namespace unapec.Components
{
    partial class AccountingEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.secondAddBtn = new System.Windows.Forms.Button();
            this.targetAddBtn = new System.Windows.Forms.Button();
            this.secondAccountsDataGrid = new System.Windows.Forms.DataGridView();
            this.targetAccountsDataGrid = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.secondAmount = new System.Windows.Forms.TextBox();
            this.secondAccountLabel = new System.Windows.Forms.Label();
            this.secondAccountCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.amountField = new System.Windows.Forms.TextBox();
            this.movementTypeCombo = new System.Windows.Forms.ComboBox();
            this.accountCombo = new System.Windows.Forms.ComboBox();
            this.auxiliaryCombo = new System.Windows.Forms.ComboBox();
            this.descriptionField = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondAccountsDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.targetAccountsDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.secondAddBtn);
            this.groupBox1.Controls.Add(this.targetAddBtn);
            this.groupBox1.Controls.Add(this.secondAccountsDataGrid);
            this.groupBox1.Controls.Add(this.targetAccountsDataGrid);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.secondAmount);
            this.groupBox1.Controls.Add(this.secondAccountLabel);
            this.groupBox1.Controls.Add(this.secondAccountCombo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cancelBtn);
            this.groupBox1.Controls.Add(this.saveBtn);
            this.groupBox1.Controls.Add(this.amountField);
            this.groupBox1.Controls.Add(this.movementTypeCombo);
            this.groupBox1.Controls.Add(this.accountCombo);
            this.groupBox1.Controls.Add(this.auxiliaryCombo);
            this.groupBox1.Controls.Add(this.descriptionField);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(675, 473);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Entry";
            // 
            // secondAddBtn
            // 
            this.secondAddBtn.Location = new System.Drawing.Point(403, 162);
            this.secondAddBtn.Name = "secondAddBtn";
            this.secondAddBtn.Size = new System.Drawing.Size(77, 30);
            this.secondAddBtn.TabIndex = 20;
            this.secondAddBtn.Text = "Add";
            this.secondAddBtn.UseVisualStyleBackColor = true;
            this.secondAddBtn.Click += new System.EventHandler(this.secondAddBtn_Click);
            // 
            // targetAddBtn
            // 
            this.targetAddBtn.Location = new System.Drawing.Point(89, 162);
            this.targetAddBtn.Name = "targetAddBtn";
            this.targetAddBtn.Size = new System.Drawing.Size(67, 30);
            this.targetAddBtn.TabIndex = 19;
            this.targetAddBtn.Text = "Add";
            this.targetAddBtn.UseVisualStyleBackColor = true;
            this.targetAddBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // secondAccountsDataGrid
            // 
            this.secondAccountsDataGrid.AllowUserToAddRows = false;
            this.secondAccountsDataGrid.AllowUserToOrderColumns = true;
            this.secondAccountsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.secondAccountsDataGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.secondAccountsDataGrid.Location = new System.Drawing.Point(383, 270);
            this.secondAccountsDataGrid.MultiSelect = false;
            this.secondAccountsDataGrid.Name = "secondAccountsDataGrid";
            this.secondAccountsDataGrid.ReadOnly = true;
            this.secondAccountsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.secondAccountsDataGrid.Size = new System.Drawing.Size(238, 139);
            this.secondAccountsDataGrid.TabIndex = 18;
            this.secondAccountsDataGrid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnSecondAccountRemoved);
            // 
            // targetAccountsDataGrid
            // 
            this.targetAccountsDataGrid.AllowUserToAddRows = false;
            this.targetAccountsDataGrid.AllowUserToOrderColumns = true;
            this.targetAccountsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.targetAccountsDataGrid.Location = new System.Drawing.Point(45, 270);
            this.targetAccountsDataGrid.MultiSelect = false;
            this.targetAccountsDataGrid.Name = "targetAccountsDataGrid";
            this.targetAccountsDataGrid.ReadOnly = true;
            this.targetAccountsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.targetAccountsDataGrid.Size = new System.Drawing.Size(238, 139);
            this.targetAccountsDataGrid.TabIndex = 17;
            this.targetAccountsDataGrid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnTargetAccountRemoved);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(400, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Amount";
            // 
            // secondAmount
            // 
            this.secondAmount.Location = new System.Drawing.Point(403, 133);
            this.secondAmount.Name = "secondAmount";
            this.secondAmount.Size = new System.Drawing.Size(168, 20);
            this.secondAmount.TabIndex = 15;
            this.secondAmount.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // secondAccountLabel
            // 
            this.secondAccountLabel.AutoSize = true;
            this.secondAccountLabel.Location = new System.Drawing.Point(400, 63);
            this.secondAccountLabel.Name = "secondAccountLabel";
            this.secondAccountLabel.Size = new System.Drawing.Size(47, 13);
            this.secondAccountLabel.TabIndex = 14;
            this.secondAccountLabel.Text = "Account";
            // 
            // secondAccountCombo
            // 
            this.secondAccountCombo.FormattingEnabled = true;
            this.secondAccountCombo.Location = new System.Drawing.Point(403, 79);
            this.secondAccountCombo.Name = "secondAccountCombo";
            this.secondAccountCombo.Size = new System.Drawing.Size(168, 21);
            this.secondAccountCombo.TabIndex = 13;
            this.secondAccountCombo.SelectedIndexChanged += new System.EventHandler(this.secondAccountCombo_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(86, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Movement Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Account";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(400, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Auxiliary";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Description";
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(6, 444);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 7;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(594, 444);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 6;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // amountField
            // 
            this.amountField.Location = new System.Drawing.Point(89, 133);
            this.amountField.Name = "amountField";
            this.amountField.Size = new System.Drawing.Size(168, 20);
            this.amountField.TabIndex = 5;
            this.amountField.TextChanged += new System.EventHandler(this.amountField_TextChanged);
            // 
            // movementTypeCombo
            // 
            this.movementTypeCombo.FormattingEnabled = true;
            this.movementTypeCombo.Location = new System.Drawing.Point(89, 219);
            this.movementTypeCombo.Name = "movementTypeCombo";
            this.movementTypeCombo.Size = new System.Drawing.Size(168, 21);
            this.movementTypeCombo.TabIndex = 3;
            this.movementTypeCombo.SelectedIndexChanged += new System.EventHandler(this.movementTypeCombo_SelectedIndexChanged);
            // 
            // accountCombo
            // 
            this.accountCombo.FormattingEnabled = true;
            this.accountCombo.Location = new System.Drawing.Point(89, 79);
            this.accountCombo.Name = "accountCombo";
            this.accountCombo.Size = new System.Drawing.Size(168, 21);
            this.accountCombo.TabIndex = 2;
            this.accountCombo.SelectedIndexChanged += new System.EventHandler(this.accountCombo_SelectedIndexChanged);
            // 
            // auxiliaryCombo
            // 
            this.auxiliaryCombo.FormattingEnabled = true;
            this.auxiliaryCombo.Location = new System.Drawing.Point(403, 32);
            this.auxiliaryCombo.Name = "auxiliaryCombo";
            this.auxiliaryCombo.Size = new System.Drawing.Size(168, 21);
            this.auxiliaryCombo.TabIndex = 1;
            this.auxiliaryCombo.SelectedIndexChanged += new System.EventHandler(this.auxiliaryCombo_SelectedIndexChanged);
            // 
            // descriptionField
            // 
            this.descriptionField.Location = new System.Drawing.Point(89, 32);
            this.descriptionField.Name = "descriptionField";
            this.descriptionField.Size = new System.Drawing.Size(168, 20);
            this.descriptionField.TabIndex = 0;
            this.descriptionField.TextChanged += new System.EventHandler(this.descriptionField_TextChanged);
            // 
            // AccountingEntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(699, 497);
            this.Controls.Add(this.groupBox1);
            this.Name = "AccountingEntryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccountingEntryForm";
            this.Load += new System.EventHandler(this.AccountingEntryForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondAccountsDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.targetAccountsDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.TextBox amountField;
        private System.Windows.Forms.ComboBox movementTypeCombo;
        private System.Windows.Forms.ComboBox accountCombo;
        private System.Windows.Forms.ComboBox auxiliaryCombo;
        private System.Windows.Forms.TextBox descriptionField;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label secondAccountLabel;
        private System.Windows.Forms.ComboBox secondAccountCombo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox secondAmount;
        private System.Windows.Forms.DataGridView secondAccountsDataGrid;
        private System.Windows.Forms.DataGridView targetAccountsDataGrid;
        private System.Windows.Forms.Button secondAddBtn;
        private System.Windows.Forms.Button targetAddBtn;
    }
}