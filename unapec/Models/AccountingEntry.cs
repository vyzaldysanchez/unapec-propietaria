﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{
    public enum AccountMovementType
    {
        DB,
        CR
    }

    public class AccountingEntry : BaseModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int AuxiliaryId { get; set; }
        public AccountMovementType MovementType { get; set; }
        public String AccountingDate { get; set; }
        public double AccountingAmount { get; set; }
        public bool Registered { get; set; }
        public int AccountId { get; set; }

        #region Relations
        public virtual Account Account { get; set; }
        #endregion
    }
}
