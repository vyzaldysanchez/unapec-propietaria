﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{
    public class DBContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<CurrencyType> CurrencyTypes { get; set; }
        public DbSet<AccountingEntry> AccountingEntries { get; set; }
        public DbSet<Wholesale> Wholesales { get; set; }
        public DbSet<Params> Params { get; set; }
        public DbSet<Auxiliary> Auxiliaries { get; set; }
        public DbSet<EntryLog> EntryLog { get; set; }

        private static DBContext _instance;

        public DBContext() : base("unapecDB")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DBContext>());
        }

        public static DBContext GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DBContext();
            }

            return _instance;
        }

    }
}
