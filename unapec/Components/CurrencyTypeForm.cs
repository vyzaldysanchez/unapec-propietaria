﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Repositories;

namespace unapec.Components
{
    public partial class CurrencyTypeForm : Form
    {
        private CurrencyType _currencyType;
        private bool _edit;

        public CurrencyTypeForm(CurrencyType currencyType = null)
        {
            _edit = currencyType != null;
            _currencyType = currencyType ?? new CurrencyType();

            InitializeComponent();
        }

        private void CurrencyTypeForm_Load(object sender, EventArgs e)
        {
            if (_edit)
            {
                descriptionField.Text = _currencyType.Description;
                lastExchangeRateField.Text = _currencyType.LastExchangeRate.ToString();
                statusCheckbox.Checked = _currencyType.Status;
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lastExchangeRateField_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _currencyType.LastExchangeRate = double.Parse(lastExchangeRateField.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Only numeric values are allowed...");
                lastExchangeRateField.Text = "0";
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (descriptionField.Text == "" || lastExchangeRateField.Text == "0" || lastExchangeRateField.Text == "")
            {
                MessageBox.Show("Description and lastExchangeRate fields cannot be empty...");
                return;
            }

            DialogResult saveConfirmed = MessageBox.Show("Are you sure you want to save the currency type: " + _currencyType.Description + "?",
                "Confirm operation!", MessageBoxButtons.YesNo);

            if (saveConfirmed.Equals(DialogResult.Yes))
            {
                if (_edit)
                {
                    CurrencyTypeRepository.GetInstance().Edit(_currencyType);
                }
                else
                {
                    CurrencyTypeRepository.GetInstance().Add(_currencyType);
                }

                this.Close();
            }
        }

        private void descriptionField_TextChanged(object sender, EventArgs e)
        {
            _currencyType.Description = descriptionField.Text;
        }

        private void statusCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            _currencyType.Status = statusCheckbox.Checked;
        }
    }
}
