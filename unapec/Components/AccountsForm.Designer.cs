﻿namespace unapec.Components
{
    partial class AccountsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.descriptionTextField = new System.Windows.Forms.TextBox();
            this.accountTypeCombo = new System.Windows.Forms.ComboBox();
            this.allowTransactionsCheckbox = new System.Windows.Forms.CheckBox();
            this.levelCombo = new System.Windows.Forms.ComboBox();
            this.parentAccountCombo = new System.Windows.Forms.ComboBox();
            this.statusChekbox = new System.Windows.Forms.CheckBox();
            this.accountFormBox = new System.Windows.Forms.GroupBox();
            this.balanceTextbox = new System.Windows.Forms.TextBox();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.saveAccountBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.parentAccountLabel = new System.Windows.Forms.Label();
            this.levelLabel = new System.Windows.Forms.Label();
            this.accountTypeLabel = new System.Windows.Forms.Label();
            this.accountDescriptionLabel = new System.Windows.Forms.Label();
            this.accountFormBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // descriptionTextField
            // 
            this.descriptionTextField.Location = new System.Drawing.Point(143, 33);
            this.descriptionTextField.Name = "descriptionTextField";
            this.descriptionTextField.Size = new System.Drawing.Size(144, 20);
            this.descriptionTextField.TabIndex = 0;
            this.descriptionTextField.TextChanged += new System.EventHandler(this.DescriptionTextField_TextChanged);
            // 
            // accountTypeCombo
            // 
            this.accountTypeCombo.FormattingEnabled = true;
            this.accountTypeCombo.Location = new System.Drawing.Point(143, 59);
            this.accountTypeCombo.Name = "accountTypeCombo";
            this.accountTypeCombo.Size = new System.Drawing.Size(144, 21);
            this.accountTypeCombo.TabIndex = 1;
            this.accountTypeCombo.SelectedIndexChanged += new System.EventHandler(this.AccountTypeCombo_SelectedIndexChanged);
            // 
            // allowTransactionsCheckbox
            // 
            this.allowTransactionsCheckbox.AutoSize = true;
            this.allowTransactionsCheckbox.Location = new System.Drawing.Point(143, 86);
            this.allowTransactionsCheckbox.Name = "allowTransactionsCheckbox";
            this.allowTransactionsCheckbox.Size = new System.Drawing.Size(115, 17);
            this.allowTransactionsCheckbox.TabIndex = 2;
            this.allowTransactionsCheckbox.Text = "Allow Transactions";
            this.allowTransactionsCheckbox.UseVisualStyleBackColor = true;
            this.allowTransactionsCheckbox.CheckedChanged += new System.EventHandler(this.AllowTransactionsCheckbox_CheckedChanged);
            // 
            // levelCombo
            // 
            this.levelCombo.FormattingEnabled = true;
            this.levelCombo.Location = new System.Drawing.Point(143, 109);
            this.levelCombo.Name = "levelCombo";
            this.levelCombo.Size = new System.Drawing.Size(144, 21);
            this.levelCombo.TabIndex = 3;
            this.levelCombo.SelectedIndexChanged += new System.EventHandler(this.LevelCombo_SelectedIndexChanged);
            // 
            // parentAccountCombo
            // 
            this.parentAccountCombo.FormattingEnabled = true;
            this.parentAccountCombo.Location = new System.Drawing.Point(143, 136);
            this.parentAccountCombo.Name = "parentAccountCombo";
            this.parentAccountCombo.Size = new System.Drawing.Size(144, 21);
            this.parentAccountCombo.TabIndex = 4;
            this.parentAccountCombo.SelectedIndexChanged += new System.EventHandler(this.ParentAccountCombo_SelectedIndexChanged);
            // 
            // statusChekbox
            // 
            this.statusChekbox.AutoSize = true;
            this.statusChekbox.Location = new System.Drawing.Point(143, 189);
            this.statusChekbox.Name = "statusChekbox";
            this.statusChekbox.Size = new System.Drawing.Size(56, 17);
            this.statusChekbox.TabIndex = 6;
            this.statusChekbox.Text = "Status";
            this.statusChekbox.UseVisualStyleBackColor = true;
            this.statusChekbox.CheckedChanged += new System.EventHandler(this.StatusChekbox_CheckedChanged);
            // 
            // accountFormBox
            // 
            this.accountFormBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.accountFormBox.Controls.Add(this.balanceTextbox);
            this.accountFormBox.Controls.Add(this.cancelBtn);
            this.accountFormBox.Controls.Add(this.saveAccountBtn);
            this.accountFormBox.Controls.Add(this.label1);
            this.accountFormBox.Controls.Add(this.parentAccountLabel);
            this.accountFormBox.Controls.Add(this.levelLabel);
            this.accountFormBox.Controls.Add(this.accountTypeLabel);
            this.accountFormBox.Controls.Add(this.accountDescriptionLabel);
            this.accountFormBox.Controls.Add(this.descriptionTextField);
            this.accountFormBox.Controls.Add(this.statusChekbox);
            this.accountFormBox.Controls.Add(this.accountTypeCombo);
            this.accountFormBox.Controls.Add(this.allowTransactionsCheckbox);
            this.accountFormBox.Controls.Add(this.parentAccountCombo);
            this.accountFormBox.Controls.Add(this.levelCombo);
            this.accountFormBox.Location = new System.Drawing.Point(12, 12);
            this.accountFormBox.Name = "accountFormBox";
            this.accountFormBox.Size = new System.Drawing.Size(319, 269);
            this.accountFormBox.TabIndex = 7;
            this.accountFormBox.TabStop = false;
            this.accountFormBox.Text = "Manage Account";
            // 
            // balanceTextbox
            // 
            this.balanceTextbox.Location = new System.Drawing.Point(143, 165);
            this.balanceTextbox.Name = "balanceTextbox";
            this.balanceTextbox.Size = new System.Drawing.Size(144, 20);
            this.balanceTextbox.TabIndex = 13;
            this.balanceTextbox.TextChanged += new System.EventHandler(this.BalanceTextbox_TextChanged);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(49, 230);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 12;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // saveAccountBtn
            // 
            this.saveAccountBtn.Location = new System.Drawing.Point(192, 230);
            this.saveAccountBtn.Name = "saveAccountBtn";
            this.saveAccountBtn.Size = new System.Drawing.Size(75, 23);
            this.saveAccountBtn.TabIndex = 11;
            this.saveAccountBtn.Text = "Save";
            this.saveAccountBtn.UseVisualStyleBackColor = true;
            this.saveAccountBtn.Click += new System.EventHandler(this.SaveAccountBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Balance";
            // 
            // parentAccountLabel
            // 
            this.parentAccountLabel.AutoSize = true;
            this.parentAccountLabel.Location = new System.Drawing.Point(46, 139);
            this.parentAccountLabel.Name = "parentAccountLabel";
            this.parentAccountLabel.Size = new System.Drawing.Size(81, 13);
            this.parentAccountLabel.TabIndex = 8;
            this.parentAccountLabel.Text = "Parent Account";
            // 
            // levelLabel
            // 
            this.levelLabel.AutoSize = true;
            this.levelLabel.Location = new System.Drawing.Point(46, 112);
            this.levelLabel.Name = "levelLabel";
            this.levelLabel.Size = new System.Drawing.Size(33, 13);
            this.levelLabel.TabIndex = 9;
            this.levelLabel.Text = "Level";
            // 
            // accountTypeLabel
            // 
            this.accountTypeLabel.AutoSize = true;
            this.accountTypeLabel.Location = new System.Drawing.Point(46, 62);
            this.accountTypeLabel.Name = "accountTypeLabel";
            this.accountTypeLabel.Size = new System.Drawing.Size(74, 13);
            this.accountTypeLabel.TabIndex = 8;
            this.accountTypeLabel.Text = "Account Type";
            // 
            // accountDescriptionLabel
            // 
            this.accountDescriptionLabel.AutoSize = true;
            this.accountDescriptionLabel.Location = new System.Drawing.Point(46, 36);
            this.accountDescriptionLabel.Name = "accountDescriptionLabel";
            this.accountDescriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.accountDescriptionLabel.TabIndex = 7;
            this.accountDescriptionLabel.Text = "Description";
            // 
            // AccountsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(343, 293);
            this.Controls.Add(this.accountFormBox);
            this.Name = "AccountsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccountsForm";
            this.Load += new System.EventHandler(this.AccountsForm_Load);
            this.accountFormBox.ResumeLayout(false);
            this.accountFormBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox descriptionTextField;
        private System.Windows.Forms.ComboBox accountTypeCombo;
        private System.Windows.Forms.CheckBox allowTransactionsCheckbox;
        private System.Windows.Forms.ComboBox levelCombo;
        private System.Windows.Forms.ComboBox parentAccountCombo;
        private System.Windows.Forms.CheckBox statusChekbox;
        private System.Windows.Forms.GroupBox accountFormBox;
        private System.Windows.Forms.Label accountTypeLabel;
        private System.Windows.Forms.Label accountDescriptionLabel;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button saveAccountBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label parentAccountLabel;
        private System.Windows.Forms.Label levelLabel;
        private System.Windows.Forms.TextBox balanceTextbox;
    }
}