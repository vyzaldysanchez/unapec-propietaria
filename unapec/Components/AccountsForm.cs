﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using unapec.Repositories;

namespace unapec.Components
{
    public partial class AccountsForm : Form
    {
        private Account _account;
        private bool _edit = false;
        private bool _initiating = false;

        public AccountsForm(Account account = null)
        {
            _edit = account != null;
            _account = account ?? new Account();

            InitializeComponent();
        }

        private void AccountsForm_Load(object sender, EventArgs e)
        {
            _initiating = true;

            List<AccountType> accountTypes = AccountTypeRepository.GetInstance().GetAll().ToList();
            List<ComboItemInt> accountSource = new List<ComboItemInt>();

            foreach (AccountType type in accountTypes)
            {
                accountSource.Add(new ComboItemInt() { Name = type.Description, Value = type.Id });
            }

            SetComboSource(accountTypeCombo, accountSource);

            List<ComboItemLevel> levelSource = new List<ComboItemLevel>
            {
                new ComboItemLevel() { Name = "Parent", Value = Level.Parent},
                new ComboItemLevel() { Name = "Sub Parent", Value = Level.SubParent },
                new ComboItemLevel() { Name = "Child", Value = Level.Child }
            };

            SetComboSource(levelCombo, levelSource);

            List<Account> parentAccounts = AccountRepository.GetInstance().GetParentAccounts().ToList();

            List<ComboItemInt> parentAccountsSource = new List<ComboItemInt>();

            foreach (Account account in parentAccounts)
            {
                parentAccountsSource.Add(new ComboItemInt() { Name = account.Description, Value = account.Id });
            }

            SetComboSource(parentAccountCombo, parentAccountsSource);

            if (_edit)
            {
                descriptionTextField.Text = _account.Description;
                accountTypeCombo.SelectedValue = _account.AccountTypeId;
                allowTransactionsCheckbox.Checked = _account.AllowTransactions;
                levelCombo.SelectedValue = _account.Level;
                parentAccountCombo.SelectedValue = (int)_account.ParentAccountId;
                balanceTextbox.Text = _account.Balance.ToString();
                statusChekbox.Checked = _account.Status;
            }

            _initiating = false;
        }

        private void SetAccountType()
        {
            if (accountTypeCombo.SelectedValue != null)
            {
                _account.AccountTypeId = (accountTypeCombo.SelectedItem as ComboItemInt).Value;
            }
        }

        private void SetComboSource(ComboBox combo, IList source)
        {
            combo.DataSource = source;
            combo.DisplayMember = "Name";
            combo.ValueMember = "Value";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveAccountBtn_Click(object sender, EventArgs e)
        {
            if (_account.Description == "" || _account.AccountTypeId == 0 || _account.Balance == 0)
            {
                MessageBox.Show("Some required fields are empty...");
                return;
            }

            var saveConfirmed = MessageBox.Show("Are you sure you want to save the account: " + _account.Description + "?",
                "Confirm operation!", MessageBoxButtons.YesNo);

            if (saveConfirmed == DialogResult.Yes)
            {
                if (_account.Level.Equals(Level.Parent) || _account.Level.Equals(Level.SubParent))
                {
                    _account.AllowTransactions = false;
                }

                if (_edit)
                {
                    AccountRepository.GetInstance().Edit(_account);
                }
                else
                {
                    AccountRepository.GetInstance().Add(_account);
                }

                this.Close();
            }
        }

        private void AccountTypeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_initiating)
            {
                SetAccountType();
            }
        }

        private void DescriptionTextField_TextChanged(object sender, EventArgs e)
        {
            _account.Description = descriptionTextField.Text;
        }

        private void AllowTransactionsCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (!_initiating)
            {
                _account.AllowTransactions = allowTransactionsCheckbox.Checked;
            }
        }

        private void LevelCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_initiating)
            {
                _account.Level = (levelCombo.SelectedItem as ComboItemLevel).Value;
            }
        }

        private void StatusChekbox_CheckedChanged(object sender, EventArgs e)
        {
            _account.Status = statusChekbox.Checked;
        }

        private void ParentAccountCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_initiating)
            {
                _account.ParentAccountId = (parentAccountCombo.SelectedItem as ComboItemInt).Value;
            }
        }

        private void BalanceTextbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _account.Balance = long.Parse(balanceTextbox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Only numeric values are allowed...");
                balanceTextbox.Text = "0";
            }
        }
    }

    internal class ComboItemString
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    internal class ComboItemInt : ComboItemString
    {
        public new string Name { get; set; }
        public new int Value { get; set; }
    }

    internal class ComboItemLevel : ComboItemString
    {
        public new string Name { get; set; }
        public new Level Value { get; set; }
    }

    internal class ComboItemAccountMovement : ComboItemString
    {
        public new string Name { get; set; }
        public new AccountMovementType Value { get; set; }
    }
}
