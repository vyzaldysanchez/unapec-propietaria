﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unapec.Models;

namespace unapec
{ 
    public class CurrencyType : BaseModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double LastExchangeRate { get; set; }
        public bool Status { get; set; }
    }
}
